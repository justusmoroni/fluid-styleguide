<?php
namespace Leonmrni\FluidStyleguide\Tests\Service;

use Leonmrni\FluidStyleguide\Service\ConfigurationService;
use PHPUnit\Framework\TestCase;

class ConfigurationServiceTest extends TestCase
{
    /**
     * @test
     */
    public function itsPossibleToReadASingleConfigurationFile()
    {
        $subject = new ConfigurationService(
            __DIR__ . DIRECTORY_SEPARATOR . implode(DIRECTORY_SEPARATOR, [
                '..', 'Fixtures', 'Service', 'Valid'
            ])
        );

        $this->assertEquals(
            require __DIR__ . DIRECTORY_SEPARATOR . implode(DIRECTORY_SEPARATOR, [
                '..', 'Fixtures', 'Service', 'ExpectedConfigurationService.php'
            ]),
            $subject->getConfiguration()
        );
    }

    /**
     * @test
     * @expectedException \Symfony\Component\Yaml\Exception\ParseException
     */
    public function throwExceptionWhenConfigurationCannotBeParsed()
    {
        new ConfigurationService(
            __DIR__ . DIRECTORY_SEPARATOR . implode(DIRECTORY_SEPARATOR, [
                '..', 'Fixtures', 'Service', 'Invalid'
            ])
        );
    }
}
