<?php
namespace Leonmrni\FluidStyleguide\Service;

/*
 * MIT License
 *
 * Copyright (c) [2017] [Leonmrni]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Finder\Finder;
use TYPO3Fluid\Fluid\Exception;

/**
 * @author Leonmrni <developer@leonmrni.com>
 */
class ConfigurationService
{
    /**
     * @var array $configuration
     */
    protected $configuration = [];

    /**
     * @var Finder $finder
     */
    protected $finder;

    /**
     * @param string $path
     *
     * @return ConfigurationService
     */
    public function __construct($path)
    {
        $this->finder = new Finder();
        $this->finder->name('*.yml');
        $this->finder->files()->in($path);

        $this->readConfiguration();

        return $this;
    }

    /**
     * Read the configuration files and save them.
     *
     * @return void
     */
    protected function readConfiguration()
    {
        foreach ($this->finder as $file) {
            $this->configuration[$this->clearFileName($file)]
                = Yaml::parse($file->getContents());
        }
    }

    /**
     * Returns the yaml configuration.
     *
     * @return array
     */
    public function getConfiguration()
    {
        return $this->configuration;
    }

    /**
     * Remove type from file name.
     *
     * @param \SplFileInfo $file
     *
     * @return string
     */
    protected function clearFileName(\SplFileInfo $file)
    {
        return $file->getBasename('.' . $file->getExtension());
    }
}
